using JLQCD
using Documenter

DocMeta.setdocmeta!(JLQCD, :DocTestSetup, :(using JLQCD); recursive=true)

makedocs(;
    modules=[JLQCD],
    authors="T Shelton",
    repo="https://gitlab.com/tshelton/JLQCD.jl/blob/{commit}{path}#{line}",
    sitename="JLQCD.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://tshelton.gitlab.io/JLQCD.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)

module GenerateMatrices
    using LinearAlgebra
    using Random
    using Distributions: Uniform
    using FromFile: @from
    @from "unitarize.jl" import Unitarize: specialunitarize

    export generaterandomSUNmatrices
    """
    generatematrices(N,number,ϵ)

    Return a vector of 'number' random, special-unitary matrices.

    These should approximately span SU('N'), with an 'ϵ' Monte Carlo spacing
    controlling how from from unit they are (i.e., update size for the 
    Metropolis algorithm). For any element of the array, its
    inverse is also included, so there are 'number'/2 independent variables.
    """
    function generaterandomSUNmatrices(N::Integer,number::Integer,ϵ::Real)
        
        matrices = Vector{Matrix{Complex{Float64}}}(undef,number)

        for j in 1:2:number # Half the elements are just inverses
            H = Hermitian(rand(Uniform(-1,1),N,N)) # Generate random NxN hermitean matrix
            U = specialunitarize(I+ϵ*1im*H) # Unitarize 1 + iϵH
            matrices[j] = U
            matrices[j+1] = U'
        end

        return matrices
    end
end
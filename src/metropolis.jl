module Metropolis
    using Random: rand
    using LinearAlgebra: I, tr
    using Mocking
    using FromFile: @from
    @from "lattice.jl" import Lattice: GaugeFields
    @from "params.jl" import Parameters: Params
    @from "generatematrices.jl" import GenerateMatrices: generaterandomSUNmatrices
    @from "operators.jl" import Operators: plaquettestaples, improvedstaples

    export localwilsonaction, update!, sweep!
    
    function localwilsonaction(U::AbstractMatrix, Γ::AbstractMatrix, 
        M::AbstractMatrix, params::Params)

        return -(params.β / 3.0) * real(tr((M - I) * U * Γ))
    end

    function update!(lattice::GaugeFields, Γ::AbstractMatrix, M::AbstractMatrix,
        x::CartesianIndex, μ::Integer, params::Params)

        ΔS = localwilsonaction(lattice.links[x][μ], Γ , M, params)
        #ΔS = params.β*real(tr((I - M)*lattice.links[x][μ]*Γ))

        if exp(-ΔS) > @mock rand() 
        #if ΔS < 0 || exp(-ΔS) > rand()
            lattice.links[x][μ] = M *lattice.links[x][μ]
        end#
        
        return nothing
    end

    function sweep!(lattice::GaugeFields, matrices::AbstractArray, 
        params::Params)

        Γ = zero(matrices[begin]) # Local staples
        M = zero(matrices[begin]) # Gauge group updater

        for x in CartesianIndices(lattice.links)
            for μ in eachindex(lattice.links[x])
                Γ = plaquettestaples(lattice, x, μ)
                for _ in 1:params.N_hits
                    M = matrices[rand(begin : end)]

                    update!(lattice, Γ, M, x, μ, params)
                end
            end
        end
        return nothing
    end

    function sweepimproved!(lattice::GaugeFields, matrices::AbstractArray,
        params::Params)
        
        Γ = zero(matrices[begin])
        M = zero(matrices[begin])
        
        for x in CartesianIndices(lattice.links)
            for μ in eachindex(lattice.links[x])
                Γ = improvedstaples(lattice, x, μ, params)
                for _ in 1:params.N_hits
                    M = matrices[rand(begin : end)]
    
                    update!(lattice,Γ,M,x,μ,params)
                end
            end
        end
        return nothing
    end

end #module Metropolis
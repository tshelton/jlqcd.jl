module Lattice
    using LinearAlgebra

    #export GaugeFields
    """
    nearestneighbors(index, L)

    Compute and return the 2*length(index) nearest neighbors (as a vector of 
    CartesianIndices) of the point 'index' in an 'L'^length(index) isotropic 
    hypercubic lattice.
    """
    function nearestneighbors(index::CartesianIndex,L::Integer)
        dim = length(index)
        indicesplus = Vector{typeof(index)}(undef,dim)
        indicesminus = Vector{typeof(index)}(undef,dim)
        for n in eachindex(indicesplus)
            # temporary vector of original index to do cacluations on
            plus = collect(Tuple(index))
            minus = collect(Tuple(index))
            # Next and previous index for current dimension
            plus[n] = mod1(plus[n]+1,L)
            minus[n] = mod1(minus[n]-1,L)
            indicesplus[n] = typeof(index)(plus...)
            indicesminus[n] = typeof(index)(minus...)
        end
        #new(braid(indicesplus,indicesminus))
        return vcat(indicesplus,indicesminus)
    end

    """
    GaugeFields{T<:CartesianIndex,U<:AbstractMatrix}

    Hold nearest-neighbors look-up table and gauge fields defined on the links of a lattice.

    # Fields
    - `nearestneighbors::Array{Vector{T}}`: An L^lat_dim array of 2*lat_dim Vectors 
        of CartesianIndex{lat_dim}'s for each of the neighboring points of each vertex. 
    - `links::Array{Vector{U}}`: An L^lattice_dimension array of lat_dim Vectors 
        of Matrices (the gauge fields) for each of the links, represented as the links from each vertex
            noting that only positive directions are independent variables.
    - `directions::UnitRange{Integer}`: Pre-allocation of directions used in some functions.

    # Constructor Arguments
    - `N_lat::Integer`: Temporal + spacial dimensions of lattice (usually four)
    - `N::Integer`: SU(N) for the lattice link gauge fields
    - `L::Integer`: ``L/a`` points on a side of the lattice; assumed isotropic hypercubic 
    - `mat_type`: Optional eltype of links, defaults to Matrix{ComplexF64} for SU(N)
    """
    struct GaugeFields{T<:CartesianIndex,U<:AbstractMatrix}
        nearest_neighbors::Array{Vector{T}}
        links::Array{Vector{U}}
        directions::UnitRange{Integer}
        
        function GaugeFields(N_lat::Integer, N::Integer, L::Integer, 
            mat_type = Matrix{ComplexF64})

            directions = 1:N_lat

            nearest_neighbors = Array{Vector{CartesianIndex{N_lat}},
                N_lat}(undef,fill(L,N_lat)...)
            links = Array{Vector{mat_type},N_lat}(undef,fill(L,N_lat)...)

            for index in CartesianIndices(nearest_neighbors)
                nearest_neighbors[index] = nearestneighbors(index,L)
                links[index] = [mat_type(I,N,N) for _ in 1:N_lat]
                #for direction in directions
                #links[index] = fill(Matrix{ComplexF64}(I,N,N),N_lat)
                #links[index][direction] = Matrix{ComplexF64}(I,N,N)
                #end
            end

            return new{CartesianIndex{N_lat},mat_type}(nearest_neighbors,
                links,directions)
        end
    end

end
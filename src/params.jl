module Parameters
    using DelimitedFiles
    #=
    Params{T<:AbstractFloat,Q<:Integer}

    Hold parameters for Monte Carlo Gluonic Integration

    # Arguments
    - `β::T`: ``\\tilde{β}/u_0^4`` (for the unimproved Wilson action)
    - `ϵ::T`: Monte Carlo spacing
    - `lattice_dimension::Q`: temporal + spacial dimensions (usually 4)
    - `side_length`: L/a points on a side of the lattice; assumed isotropic hypercubic 
    - `matrix_dimension`: SU(N='matrix_dimension') gauge theory
    - `number_matrices`: Number of random matrices to draw from when updating (includes inverses)
    - `N_cor`: Number of steps to update between measurements to reduce autocorrelation
    - `N_cf`: Number of configurations to sample
    - `N_hits`: Number of "hits" for each link
    =#
    struct Params{T<:AbstractFloat,Q<:Integer}
        a::T # Lattice spacing. May not actually be used; TODO check
        β::T
        u0::T
        ϵ::T
        lattice_dimension::Q
        side_length::Q
        matrix_dimension::Q
        number_matrices::Q
        N_cor::Q
        N_cf::Q
        N_hits::Q
        # Implicit constructor
        Params{T,Q}(a::T, β::T, u0::T, ϵ::T, lattice_dimension::Q, 
            side_length::Q, matrix_dimension::Q, number_matrices::Q, N_cor::Q,
            N_cf::Q, N_hits::Q) where {T<:AbstractFloat,Q<:Integer} = new(a, β, 
            u0, ϵ, lattice_dimension, side_length, matrix_dimension,
            number_matrices, N_cor, N_cf, N_hits)
        # Extra constructor to read parameters from a file
        function Params(file_name::String)
            parameters = readdlm(file_name)
            return new{Float64,Int64}(parameters...)
        end
    end

    
end
module Operators
    using LinearAlgebra
    using FromFile: @from
    @from "lattice.jl" import Lattice: GaugeFields
    @from "trmul.jl" import Trmul: trmul
    @from "params.jl" import Parameters: Params

    export plaquetteoperator, rectangleoperator, planarrectangularoperator, 
        measureplaquette, measurerectangle, measureplanarrectangular,
        plaquettestaples, rectanglestaples, improvedstaples


    """
    plaquetteoperator(lattice, x, μ, ν)

    Return the plaquette operator, 
        ``P_{μν}(x) ≔ \\frac{1}{3}\\Re\\tr(U_{μ}(x)U_{ν}(x+μ)U_{μ}(x+ν)^{†}U_{ν}(x)^{†})``
    from the vertex 'x' of 'lattice' in the 'μ''ν'-plane.

    # Example
    #```jldoctest
    julia> lattice = Lattice(4,3,8);
    julia> x = CartesianIndex{4}(1,1,1,1);
    julia> μ = 1; ν = 2;
    julia> plaquetteoperator(lattice, x, μ, ν)
    1.0
    #```
    """
    function plaquetteoperator(lattice::GaugeFields, x::CartesianIndex, 
        μ::Integer, ν::Integer)
        # Get neighboring sites in the positive μ and ν directions
        xplusμ = lattice.nearest_neighbors[x][μ]
        xplusν = lattice.nearest_neighbors[x][ν]

        return real(trmul( lattice.links[x][μ],lattice.links[xplusμ][ν],
            lattice.links[xplusν][μ]',lattice.links[x][ν]' ))/3
    end


    """
    rectangleoperator(lattice, x, μ, ν)

    Return the 2a×a rectangle operator, 
    from the vertex 'x' of 'lattice' in the 'μ''ν'-plane.

    # Example
    ```jldoctest
    julia> lattice = Lattice(4,3,8);
    julia> x = CartesianIndex{4}(1,1,1,1);
    julia> μ = 1; ν = 2;
    julia> rectangleoperator(lattice, x, μ, ν)
    TODO
    ```
    """
    #    ``R_{μν} ≔ \\Re\\mathop{\\text{tr}}( U_{ν}(x)U_{μ}(x+\\hat{ν})U_{μ}(x+\\hat{ν}+\hat{μ})U_{ν}^†(x+2\\hat{μ})U_{μ}^†(x+\\hat{μ})U_{μ}^†(x) ) ``

    function rectangleoperator(lattice::GaugeFields, x::CartesianIndex,
        μ::Integer, ν::Integer)
    # Get neighboring sites in the positive μ and ν directions
    xplusμ = lattice.nearest_neighbors[x][μ]
    xplusν = lattice.nearest_neighbors[x][ν]
    xplusνplusμ = lattice.nearest_neighbors[xplusν][μ]
    xplus2μ = lattice.nearest_neighbors[xplusμ][μ]

    return real(trmul(lattice.links[x][ν],lattice.links[xplusν][μ],
            lattice.links[xplusνplusμ][μ],lattice.links[xplus2μ][ν]',
            lattice.links[xplusμ][μ]',lattice.links[x][μ]'
        ))/3
    end


    function wrapindex(x::CartesianIndex)
        return CartesianIndex( mod1.(Tuple(x),3) )
    end

    function newindex(lattice::GaugeFields, x::CartesianIndex, α::Integer, 
            β::Integer, μ::Integer, ν::Integer)

        newindex = x # Immutable objects are safe to assign
        
        # Traverse along the nearest neighbors to x + α̂μ
        for _ in 1:α
            newindex = lattice.nearest_neighbors[newindex][μ]
        end
        # Traverse along the nearest neighbors to x + α̂μ + β̂ν
        for _ in 1:β
            newindex = lattice.nearest_neighbors[newindex][ν]
        end

        return newindex
    end

    """
    planarrectangularoperator(lattice, x, μ, ν, b, a)

    Return the b×a rectangle operator, 
    from the vertex 'x' of 'lattice' in the 'μ''ν'-plane.

    Highly inefficient in the number of recalculated indices and PBC cacluations.
    """
    function planarrectangularoperator(lattice::GaugeFields,x::CartesianIndex,
        μ::Integer, ν::Integer, b::Integer, a::Integer)

        μdir = CartesianIndex([j == μ ? 1 : 0 for j in 1:length(x)]...)
        νdir = CartesianIndex([j == ν ? 1 : 0 for j in 1:length(x)]...)

        Λ = copy(lattice.links[x][μ])
        # Traverse μ direction        
        for λ in 1:b-1
            Λ *= lattice.links[newindex(lattice,x,λ,0,μ,ν)][μ]
        end
        # Traverse ν direction
        for λ in 0:a-1
            Λ *= lattice.links[newindex(lattice,x, b, λ, μ, ν)][ν]
        end
        # Traverse -μ direction
        for λ in 1:b
            Λ *= lattice.links[newindex(lattice,x, b-λ, a, μ, ν)][μ]'
        end
        # Traverse -ν direction
        for λ in 1:a 
            Λ *= lattice.links[newindex(lattice,x, 0, a-λ, μ, ν)][ν]'
        end
        return real(tr(Λ))/3
    end

    function measureplaquette(lattice::GaugeFields)
        sum = 0.0
        for x in CartesianIndices(lattice.links)
            for μ in lattice.directions
                for ν in 1:μ-1
                    sum += plaquetteoperator(lattice,x,μ,ν)
                end
            end
        end
        return sum
    end

    function measurerectangle(lattice::GaugeFields)
        sum = 0.0
        for x in CartesianIndices(lattice.links)
            for μ in lattice.directions
                for ν in 1:μ-1
                    sum += rectangleoperator(lattice,x, μ,ν)
                end
            end
        end
        return sum
    end

    function measureplanarrectangular(lattice::GaugeFields,
            b::Integer, a::Integer)
        sum = 0.0
        for x in CartesianIndices(lattice.links)
            for μ in lattice.directions
                for ν in 1:μ-1
                    sum += planarrectangularoperator(lattice, x, μ, ν, b, a)
                end
            end
        end
        return sum
    end

    function plaquettestaplesold(lattice::GaugeFields, x::CartesianIndex, μ::Integer)
        νs = filter(ν -> ν != μ, 1:length(x))
    
        xpμ = lattice.nearest_neighbors[x][μ] # Doesn't change (only one)
        xpν = zero(xpμ)
        xmν = zero(xpμ)
        xpμmν = zero(xpμ)
    
        Γ = zero(lattice.links[x][μ])
    
        for ν in νs 
            xpν = lattice.nearest_neighbors[x][ν]
            xmν = lattice.nearest_neighbors[x][ν+length(x)]
            xpμmν = lattice.nearest_neighbors[xpμ][ν+length(x)]
    
            Γ += (lattice.links[xpμ][ν]*lattice.links[xpν][μ]'*lattice.links[x][ν]'
                + lattice.links[xpμmν][ν]'*lattice.links[xmν][μ]'*lattice.links[xmν][ν])
        end
    
        return Γ
    end

    function plaquettestaples(lattice::GaugeFields, x::CartesianIndex, 
            μ::Integer)
        νs = filter(ν -> ν != μ, 1:length(x))
    
        xpμ = lattice.nearest_neighbors[x][μ] # Doesn't change (only one)
        xpν = zero(xpμ)
        xmν = zero(xpμ)
        xpμmν = zero(xpμ)
    
        Γ = zero(lattice.links[x][μ])
        temp = similar(Γ)

        for ν in νs 
            xpν = lattice.nearest_neighbors[x][ν]
            xmν = lattice.nearest_neighbors[x][ν+length(x)]
            xpμmν = lattice.nearest_neighbors[xpμ][ν+length(x)]
    
            mul!(temp,lattice.links[x][ν],lattice.links[xpν][μ])
    
            mul!(Γ, lattice.links[xpμ][ν], temp',1.0,1.0)
    
            mul!(temp,lattice.links[xmν][μ],lattice.links[xpμmν][ν])
    
            mul!(Γ,temp',lattice.links[xmν][ν],1.0,1.0)
        end
    
        return Γ
    end 

    function rectanglestaples(lattice::GaugeFields, x::CartesianIndex, 
            μ::Integer)
        νs = filter(ν -> ν != μ, 1:length(x))
    
        xpμ = lattice.nearest_neighbors[x][μ] # Doesn't change
        xp2μ = lattice.nearest_neighbors[xpμ][μ]
        xpμpν = zero(xpμ)
        xpμmν = zero(xpμ)
        xpν = zero(xpμ)
        xp2ν = zero(xpμ)
        xpνmμ = zero(xpμ)
        xp2μmν = zero(xpμ)
        xmμ = lattice.nearest_neighbors[x][μ+length(x)]
        xmν = zero(xpμ)
        xm2ν = zero(xpμ)
        xmνmμ = zero(xpμ)
        xpμm2ν = zero(xpμ)
        
        Γ = zero(lattice.links[x][μ])
        for ν in νs 
            xpν = lattice.nearest_neighbors[x][ν]
            xp2ν = lattice.nearest_neighbors[xpν][ν]
            xmν = lattice.nearest_neighbors[x][ν+length(x)]
            xm2ν = lattice.nearest_neighbors[xmν][ν+length(x)]
            xpμmν = lattice.nearest_neighbors[xpμ][ν+length(x)]
            xp2μmν = lattice.nearest_neighbors[xp2μ][ν+length(x)]
            xpμpν = lattice.nearest_neighbors[xpμ][ν]
            xpνmμ = lattice.nearest_neighbors[xpν][μ+length(x)]
            xmνmμ = lattice.nearest_neighbors[xmν][μ+length(x)]
            xpμm2ν = lattice.nearest_neighbors[xpμmν][ν+length(x)]
            
            Γ += (lattice.links[xpμ][μ]*lattice.links[xp2μ][ν]*lattice.links[xpμpν][μ]'
                    *lattice.links[xpν][μ]'*lattice.links[x][ν]')
    
            Γ += (lattice.links[xpμ][μ]*lattice.links[xp2μmν][ν]'*lattice.links[xpμmν][μ]'
                *lattice.links[xmν][μ]'*lattice.links[xmν][ν])
    
            Γ += (lattice.links[xpμ][ν]*lattice.links[xpν][μ]'*lattice.links[xpνmμ][μ]'
                *lattice.links[xmμ][ν]'*lattice.links[xmμ][μ])
    
            Γ += (lattice.links[xpμmν][ν]'*lattice.links[xmν][μ]'*lattice.links[xmνmμ][μ]'
                *lattice.links[xmνmμ][ν]*lattice.links[xmμ][μ])
    
            Γ += (lattice.links[xpμmν][ν]'*lattice.links[xpμm2ν][ν]'*lattice.links[xm2ν][μ]'
                *lattice.links[xm2ν][ν]*lattice.links[xmν][ν])
    
            Γ += (lattice.links[xpμ][ν]*lattice.links[xpμpν][ν]*lattice.links[xp2ν][μ]'
                *lattice.links[xpν][ν]'*lattice.links[x][ν]')
        end
    
        return Γ
    end

    function improvedstaples(lattice::GaugeFields,x::CartesianIndex,
        μ::Integer, params::Params)
        coef1 = 5.0 / (3.0 * params.u0^4)
        coef2 = 1.0 / (12.0 * params.u0^6)
    
        ΓP = plaquettestaples(lattice,x,μ)
        ΓR = rectanglestaples(lattice,x,μ)
    
        return coef1 * ΓP - coef2 * ΓR
    end
end
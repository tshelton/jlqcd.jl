module JLQCD
    using FromFile: @from 
    @from "./generatematrices.jl" import GenerateMatrices: generaterandomSUNmatrices

    export generaterandomSUNmatrices
    
    @from "./lattice.jl" import Lattice: GaugeFields

    export GaugeFields

    @from "./metropolis.jl" import Metropolis: sweep!, sweepimproved!
    
    export sweep!, sweepimproved!
    
    @from "./operators.jl" import Operators: measureplaquette, measurerectangle,
        measureplanarrectangular

    export measureplaquette, measurerectangle, measureplanarrectangular

    @from "./params.jl" import Parameters: Params

    export Params 

    #@from "./trmul.jl" import Trmul

    #@from "./unitarize.jl" import Unitarize 
    
end #module JLQCD

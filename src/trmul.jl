module Trmul
    export trmul
    
    function trmul(A, B)
        out = zero(promote_type(eltype(A),eltype(B)))
        @inbounds for j in axes(A,1), i in axes(A,1)
            out += A[j,i]*B[i,j]
        end
        return out
    end
    """
    trmul(A,B,C)

    Return tr(A⋅B⋅C), where 'A', 'B', and 'C' are assumed to be square 
    matrices of the same dimensions and eltypes.
    """
    function trmul(A, B, C)
    # Just assume typeof(A) == typeof(B) etc, can change later if ever needed.
    #@boundscheck size(A) == size(B) && size(A) == size(C) && size(A) == size(D) || throw(BoundsError())
    #out = zero(promote_type(eltype(A),eltype(B),eltype(C),eltype(D)))
    out = zero(promote_type(eltype(A),eltype(B),eltype(C)))
    @inbounds for j in axes(A,1), i in axes(A,1), k in axes(A,1) 
        out += A[j,i]*B[i,k]*C[k,j]
    end
    return out
    end

    """
    trmul(A,B,C,D)

    Return tr(A⋅B⋅C⋅D), where 'A', 'B', 'C', and 'D' are assumed to be square 
    matrices of the same dimensions and eltypes.
    """
    function trmul(A, B, C, D)
        out = zero(promote_type(eltype(A),eltype(B),eltype(C),eltype(D)))
        @inbounds for j in axes(A,1), i in axes(A,1), k in axes(A,1), m in axes(A,1) 
            out += A[j,i]*B[i,k]*C[k,m]*D[m,j]
        end
        return out
    end

    # With six matrices, this starts to be slower by a factor of four. Still no allocs or memory, TODO: investigate.
    function trmul(A, B, C, D, E, F)
        out = zero(promote_type(eltype(A),eltype(B),eltype(C),eltype(D),eltype(E),eltype(F)))
        @inbounds for j in axes(A,1), i in axes(A,1), k in axes(A,1), m in axes(A,1), n in axes(A,1), l in axes(A,1) 
            out += A[j,i]*B[i,k]*C[k,m]*D[m,n]*E[n,l]*F[l,j]
        end
        return out
    end
end
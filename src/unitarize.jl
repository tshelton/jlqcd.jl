module Unitarize
    using LinearAlgebra
    """
    unitarize(Z)

    Return the unique¹ unitarization of a matrix 'Z'.

    ¹See F. Mezzadri. "How to generate random matrices from the classical compact groups," 
    Notices of the AMS, Volume 54, Issue 5, pp. 592-604, 2007.
    """
    function unitarize(Z)
        QR = qr(Z)
        Λ = Diagonal(QR.R)
        Λ /= abs.(Λ)
        Q = QR.Q*Λ
        return Q
    end

    """
    specialunitarize(Z)

    Return the Q = unitarize(Z) rescaled to have unit determinant.
    """
    function specialunitarize(Z)
        Q = unitarize(Z)
        return Q./complex(det(Q))^(1/size(Q,1))
    end
end
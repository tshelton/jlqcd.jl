module TestTrmul
    using Test
    using Random
    using LinearAlgebra
    using FromFile: @from 
    @from "../src/trmul.jl" import Trmul: trmul

    #Test sets based on matmul in LinearAlgebra.jl.
    
    @testset "trmul zero dimensions" begin
        # We assume that indices are inbounds, throws error is zero dimensional
        @test Matrix{Float64}(undef, 5, 0) |> t -> trmul(t',t) == 0.0
        @test_throws BoundsError Matrix{Float64}(undef, 5, 0) |> t -> trmul(t,t') == 0.0
        @test Matrix{ComplexF64}(undef, 5, 0) |> t -> trmul(t',t) == complex(0.0)
        @test_throws BoundsError Matrix{ComplexF64}(undef, 5, 0) |> t -> trmul(t,t') == complex(0.0)
    end

    @testset "2x2 trmul" begin
        AA = [1 2; 3 4]
        BB = [5 6; 7 8]
        CC = [9 10; 11 12]
        DD = [13 14; 15 16]
        EE = [17 18; 19 20]
        FF = [21 22; 23 24]
        AAi = AA + (0.5*im) .* BB
        BBi = BB + (2.5*im) .* AA[[2,1],[2,1]]
        CCi = CC + (4.5*im) .* AA[[2,1],[2,1]]
        DDi = DD + (6.5*im) .* AA[[2,1],[2,1]]
        EEi = EE + (8.5*im) .* AA[[2,1],[2,1]]
        FFi = FF + (10.5*im) .* AA[[2,1],[2,1]]

        @testset "Two matrices" begin
            for A in (copy(AA), view(AA, 1:2, 1:2)), B in (copy(BB), view(BB, 1:2, 1:2))
                @test trmul(A,B) == 69
                @test trmul(transpose(A), B) == 70
                @test trmul(A,transpose(B)) == 70
                @test trmul(transpose(A),transpose(B)) == 69
            end
            for Ai in (copy(AAi), view(AAi, 1:2, 1:2)), Bi in (copy(BBi), view(BBi, 1:2, 1:2))
                @test trmul(Ai,Bi) == -7.25 + 139.0im
                @test trmul(adjoint(Ai),Bi) == 145.0 - 37.0im
                @test trmul(Ai,adjoint(Bi)) == 145.0 + 37.0im
                @test trmul(adjoint(Ai),adjoint(Bi)) == -7.25 - 139.0im
            end
        end
        @testset "Three matrices" begin
            @test trmul(AA,BB,CC) == 1443
            @test trmul(transpose(AA), BB, CC) == 1472
            @test trmul(AA,transpose(BB),CC) == 1432
            @test trmul(transpose(AA),transpose(BB),transpose(CC)) == 1443

            @test trmul(AAi,BBi,CCi) == -3291.25 + 2512.625im
            @test trmul(adjoint(AAi),BBi,CCi) == 3668.0 + 2536.75im
            @test trmul(AAi,adjoint(BBi),CCi) == 2785.0 + 3903.25im
            @test trmul(AAi,BBi,adjoint(CCi)) == 2704.0 + 3554.5im
            @test trmul(adjoint(AAi),adjoint(BBi),adjoint(CCi)) == -3291.25 - 2512.625im
        end

        @testset "Four matrices" begin
            @test trmul(AA,BB,CC,DD) == 41777
            @test trmul(transpose(AA),BB,CC,DD) == 42724
            @test trmul(AA,transpose(BB),CC,DD) == 41460
            @test trmul(AA,BB,transpose(CC),DD) == 41764
            @test trmul(AA,BB,CC,transpose(DD)) == 42260
            @test trmul(transpose(AA),transpose(BB),transpose(CC),transpose(DD)) == 41777

            @test trmul(AAi,BBi,CCi,DDi) == -173138.9375 - 42313.5im
            @test trmul(adjoint(AAi),BBi,CCi,DDi) == 14863.125 + 191320.25im
            @test trmul(AAi,adjoint(BBi),CCi,DDi) == -53969.125 + 197906.25im
            @test trmul(AAi,BBi,adjoint(CCi),DDi) == -17350.0 + 205831.5im
            @test trmul(AAi,BBi,CCi,adjoint(DDi)) == -33927.75 + 185116.0im
            @test trmul(adjoint(AAi),adjoint(BBi),adjoint(CCi),adjoint(DDi)) == -173138.9375 + 42313.5im
        end

        @testset "Six matrices" begin
            @test trmul(AA,BB,CC,DD,EE,FF) == 69446113
            @test trmul(transpose(AA),BB,CC,DD,EE,FF) == 71190380
            @test trmul(AA,transpose(BB),CC,DD,EE,FF) == 68914316
            @test trmul(AA,BB,transpose(CC),DD,EE,FF) == 69427100
            @test trmul(AA,BB,CC,transpose(DD),EE,FF) == 69462092
            @test trmul(AA,BB,CC,DD,transpose(EE),FF) == 69464252
            @test trmul(AA,BB,CC,DD,EE,transpose(FF)) == 69998060
            @test trmul(transpose(AA),transpose(BB),transpose(CC),transpose(DD),transpose(EE),transpose(FF)) == 69446113

            @test trmul(AAi,BBi,CCi,DDi,EEi,FFi) == 3.20163888609375e8 - 6.126196291875e8im
            @test trmul(adjoint(AAi),BBi,CCi,DDi,EEi,FFi) == -7.3434998765625e8 - 1.196092171875e8im
            @test trmul(AAi,adjoint(BBi),CCi,DDi,EEi,FFi) == -6.9489772084375e8 - 3.871992629375e8im
            @test trmul(AAi,BBi,adjoint(CCi),DDi,EEi,FFi) == -7.54770147375e8 - 2.51754825375e8im
            @test trmul(AAi,BBi,CCi,adjoint(DDi),EEi,FFi) == -7.513598563125e8 - 2.07107052e8im
            @test trmul(AAi,BBi,CCi,DDi,adjoint(EEi),FFi) == -7.534187831875e8 - 1.9046052325e8im
            @test trmul(AAi,BBi,CCi,DDi,EEi,adjoint(FFi)) == -6.680979320625e8 - 2.644954515e8im
            @test trmul(adjoint(AAi),adjoint(BBi),adjoint(CCi),adjoint(DDi),adjoint(EEi),adjoint(FFi)) == 3.20163888609375e8 + 6.126196291875e8im
        end
    end
end
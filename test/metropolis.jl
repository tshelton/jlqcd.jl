module TestMetropolis
    using Test
    using Random
    using LinearAlgebra
    using Mocking
    using FromFile: @from
    @from "../src/params.jl" import Parameters: Params
    @from "../src/lattice.jl" import Lattice: GaugeFields
    @from "../src/generatematrices.jl" import GenerateMatrices: generaterandomSUNmatrices
    @from "../src/metropolis.jl" import Metropolis: localwilsonaction, update!

    Random.seed!(12345)

    test_values = (
        a = 1., # a 
        β = 5.5, # β 
        u0 = 0.0, # u₀
        ϵ = 0.24, # ϵ
        lattice_dimension = 4, # Lattice dimension
        side_length = 8, # Temporal/Spacial Length 
        matrix_dimension = 3, #  
        number_matrices = 50, #  Number of Lie Group spanning set of matrices
        N_cor = 20, # Number of runs to decorralate data
        N_cf = 400, # Number of runs 
        N_hits = 10 # Number of updates for the local link variable
    )

    params = Params{Float64,Int64}(test_values...)

    matrices = generaterandomSUNmatrices(params.matrix_dimension, 
        params.number_matrices, params.ϵ)

    LAT_DIM = 2
    MAT_DIM = 3
    L = 3

    cold_lattice = GaugeFields(LAT_DIM,MAT_DIM,L)

    lattice = GaugeFields(LAT_DIM,MAT_DIM,L)
    
    A = rand(3,3) # Garbage matrics to help assign unique and non-commuting links
    B = rand(3,3)
    f(x) = isodd(x) ? 5 : 7
    g(x) = isodd(x) ? A : B
    for (j,index) in enumerate(CartesianIndices(lattice.links))
        for (k,directionindex) in enumerate(eachindex(lattice.links[index]))
            lattice.links[index][directionindex] *= ((j+1)+f(k)*1im)*g(k)
        end
    end

    site = (x,y) -> CartesianIndex{LAT_DIM}(x,y)

    @testset "localwilsonaction" begin
        μ = 1
        x = site(2,2)

        U = lattice.links[x][μ]
        M = matrices[begin]

        Γ = (
            (7+7im)*B*conj(9+5im)*A'*conj(6+7im)*B'
            + conj(4+7im)*B'*conj(3+5im)*A'*(3+7im)*B
            )

        @test localwilsonaction(U, Γ, M, params) ≈ 
            -(params.β / 3.0) * real(tr((M - I) * U * Γ))
    end

    
    

    @testset "update!" begin
        #= Using the cold lattice because the testing one has to large 
        (unrealistic) of values, and so exp(\Delta S) is Inf otherwise.
        =#
        μ = 1
        x = site(2,2)
        U = cold_lattice.links[x][μ]
        M = matrices[begin]
        Γ = I*U
        #ΔS = 0.08365892970041927
        #exp(-ΔS) = 0.9197449002734014

        Mocking.activate()

        @testset "update! exp(-ΔS) > η" begin
            patch = @patch rand() = 0.0

            apply(patch) do
                update!(cold_lattice, Γ, M, x, μ, params)

                @test cold_lattice.links[x][μ] ≈ M*U
            end

            # Reset
            cold_lattice.links[x][μ] = U
        end
        
        @testset "update! exp(-ΔS) < η" begin
            patch = @patch rand() = 1.0

            apply(patch) do
                update!(cold_lattice, Γ, M, x, μ, params)

                @test cold_lattice.links[x][μ] == U
            end

            # Reset
            cold_lattice.links[x][μ] = U
        end
    end
end
module TestOperators
    using Test
    using Random
    using LinearAlgebra
    using FromFile: @from
    @from "../src/operators.jl" import Operators: plaquetteoperator, 
        rectangleoperator, planarrectangularoperator, 
        measureplaquette, measurerectangle, measureplanarrectangular,
        plaquettestaples, rectanglestaples, improvedstaples
    @from "../src/lattice.jl" import Lattice: GaugeFields
    @from "../src/params.jl" import Parameters: Params
    
    LAT_DIM = 2
    MAT_DIM = 3
    L = 3

    cold_lattice = GaugeFields(LAT_DIM,MAT_DIM,L)

    lattice = GaugeFields(LAT_DIM,MAT_DIM,L)
    Random.seed!(12345)
    A = rand(3,3) # Garbage matrics to help assign unique and non-commuting links
    B = rand(3,3)
    f(x) = isodd(x) ? 5 : 7
    g(x) = isodd(x) ? A : B
    for (j,index) in enumerate(CartesianIndices(lattice.links))
        for (k,directionindex) in enumerate(eachindex(lattice.links[index]))
            lattice.links[index][directionindex] *= ((j+1)+f(k)*1im)*g(k)
        end
    end

    #=

    @testset "plaquetteoperator coldstart" begin
        for (μ,ν) in [[1,2],[2,1]]
            for x in 1:L, y in 1:L
                @test plaquetteoperator(cold_lattice,
                    CartesianIndex{LAT_DIM}(x,y),μ,ν) == 1.0
            end
        end
    end

    @testset "plaquetteoperator 2D Lattice; Random Odd/Even Filling" begin
        
        μ=1
        ν=2
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(1,1),μ,ν) ≈ 
            real(tr((2+5im)*A*(3+7im)*B*conj(5+5im)*A'*conj(2+7im)*B'))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(2,1),μ,ν) ≈ 
            real(tr((3+5im)*A*(4+7im)*B*conj(6+5im)*A'*conj(3+7im)*B'))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(3,1),μ,ν) ≈ 
            real(tr((4+5im)*A*(2+7im)*B*conj(7+5im)*A'*conj(4+7im)*B'))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(1,2),μ,ν) ≈ 
            real(tr((5+5im)*A*(6+7im)*B*conj(8+5im)*A'*conj(5+7im)*B'))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(2,2),μ,ν) ≈ 
            real(tr((6+5im)*A*(7+7im)*B*conj(9+5im)*A'*conj(6+7im)*B'))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(3,2),μ,ν) ≈ 
            real(tr((7+5im)*A*(5+7im)*B*conj(10+5im)*A'*conj(7+7im)*B'))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(1,3),μ,ν) ≈ 
            real(tr((8+5im)*A*(9+7im)*B*conj(2+5im)*A'*conj(8+7im)*B'))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(2,3),μ,ν) ≈ 
            real(tr((9+5im)*A*(10+7im)*B*conj(3+5im)*A'*conj(9+7im)*B'))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(3,3),μ,ν) ≈ 
            real(tr((10+5im)*A*(8+7im)*B*conj(4+5im)*A'*conj(10+7im)*B'))/3
        
        μ=2
        ν=1
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(1,1),μ,ν) ≈ 
            real(tr(conj(2+5im)*A'*conj(3+7im)*B'*(5+5im)*A*(2+7im)*B))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(2,1),μ,ν) ≈ 
            real(tr(conj(3+5im)*A'*conj(4+7im)*B'*(6+5im)*A*(3+7im)*B))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(3,1),μ,ν) ≈ 
            real(tr(conj(4+5im)*A'*conj(2+7im)*B'*(7+5im)*A*(4+7im)*B))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(1,2),μ,ν) ≈ 
            real(tr(conj(5+5im)*A'*conj(6+7im)*B'*(8+5im)*A*(5+7im)*B))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(2,2),μ,ν) ≈ 
            real(tr(conj(6+5im)*A'*conj(7+7im)*B'*(9+5im)*A*(6+7im)*B))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(3,2),μ,ν) ≈ 
            real(tr(conj(7+5im)*A'*conj(5+7im)*B'*(10+5im)*A*(7+7im)*B))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(1,3),μ,ν) ≈ 
            real(tr(conj(8+5im)*A'*conj(9+7im)*B'*(2+5im)*A*(8+7im)*B))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(2,3),μ,ν) ≈ 
            real(tr(conj(9+5im)*A'*conj(10+7im)*B'*(3+5im)*A*(9+7im)*B))/3
        @test plaquetteoperator(lattice,CartesianIndex{LAT_DIM}(3,3),μ,ν) ≈ 
            real(tr(conj(10+5im)*A'*conj(8+7im)*B'*(4+5im)*A*(10+7im)*B))/3
        
    end
    


    @testset "rectangleoperator coldstart" begin
        for (μ,ν) in [[1,2],[2,1]]
            for x in 1:L, y in 1:L
                @test rectangleoperator(cold_lattice,
                    CartesianIndex{LAT_DIM}(x,y),μ,ν) == 1.0
            end
        end
    end

    @testset "rectangleoperator 2D Lattice; Random Odd/Even Filling" begin
        
        μ=1
        ν=2
        @test rectangleoperator(lattice,CartesianIndex{LAT_DIM}(1,1),μ,ν) ≈ 
            real(tr((2+7im)*B*(5+5im)*A*(6+5im)*A*
                conj(4+7im)*B'*conj(3+5im)*A'*conj(2+5im)*A'))/3       
        @test rectangleoperator(lattice,CartesianIndex{LAT_DIM}(2,1),μ,ν) ≈ 
            real(tr((3+7im)*B*(6+5im)*A*(7+5im)*A*
                conj(2+7im)*B'*conj(4+5im)*A'*conj(3+5im)*A'))/3
        @test rectangleoperator(lattice,CartesianIndex{LAT_DIM}(3,1),μ,ν) ≈ 
            real(tr((4+7im)*B*(7+5im)*A*(5+5im)*A*
                conj(3+7im)*B'*conj(2+5im)*A'*conj(4+5im)*A'))/3
        @test rectangleoperator(lattice,CartesianIndex{LAT_DIM}(1,2),μ,ν) ≈ 
            real(tr((5+7im)*B*(8+5im)*A*(9+5im)*A*
                conj(7+7im)*B'*conj(6+5im)*A'*conj(5+5im)*A'))/3
        @test rectangleoperator(lattice,CartesianIndex{LAT_DIM}(2,2),μ,ν) ≈ 
            real(tr((6+7im)*B*(9+5im)*A*(10+5im)*A*
                conj(5+7im)*B'*conj(7+5im)*A'*conj(6+5im)*A'))/3
        @test rectangleoperator(lattice,CartesianIndex{LAT_DIM}(3,2),μ,ν) ≈ 
            real(tr((7+7im)*B*(10+5im)*A*(8+5im)*A*
                conj(6+7im)*B'*conj(5+5im)*A'*conj(7+5im)*A'))/3
        @test rectangleoperator(lattice,CartesianIndex{LAT_DIM}(1,3),μ,ν) ≈ 
            real(tr((8+7im)*B*(2+5im)*A*(3+5im)*A*
                conj(10+7im)*B'*conj(9+5im)*A'*conj(8+5im)*A'))/3
        @test rectangleoperator(lattice,CartesianIndex{LAT_DIM}(2,3),μ,ν) ≈ 
            real(tr((9+7im)*B*(3+5im)*A*(4+5im)*A*
                conj(8+7im)*B'*conj(10+5im)*A'*conj(9+5im)*A'))/3
        @test rectangleoperator(lattice,CartesianIndex{LAT_DIM}(3,3),μ,ν) ≈ 
            real(tr((10+7im)*B*(4+5im)*A*(2+5im)*A*
                conj(9+7im)*B'*conj(8+5im)*A'*conj(10+5im)*A'))/3
        
        μ = 2
        ν = 1
        @test rectangleoperator(lattice, CartesianIndex{LAT_DIM}(1,1),μ,ν) ≈
            real(tr(
                (2+7im)*B*(5+7im)*B*(8+5im)*A*
                conj(6+7im)*B'*conj(3+7im)*B'*conj(2+5im)*A'
            ))/3
        @test rectangleoperator(lattice, CartesianIndex{LAT_DIM}(2,1),μ,ν) ≈
            real(tr(
                (3+7im)*B*(6+7im)*B*(9+5im)*A*
                conj(7+7im)*B'*conj(4+7im)*B'*conj(3+5im)*A'
            ))/3
        @test rectangleoperator(lattice, CartesianIndex{LAT_DIM}(3,1),μ,ν) ≈
            real(tr(
                (4+7im)*B*(7+7im)*B*(10+5im)*A*
                conj(5+7im)*B'*conj(2+7im)*B'*conj(4+5im)*A'
            ))/3
        @test rectangleoperator(lattice, CartesianIndex{LAT_DIM}(1,2),μ,ν) ≈
            real(tr(
                (5+7im)*B*(8+7im)*B*(2+5im)*A*
                conj(9+7im)*B'*conj(6+7im)*B'*conj(5+5im)*A'
            ))/3
        @test rectangleoperator(lattice, CartesianIndex{LAT_DIM}(2,2),μ,ν) ≈
            real(tr(
                (6+7im)*B*(9+7im)*B*(3+5im)*A*
                conj(10+7im)*B'*conj(7+7im)*B'*conj(6+5im)*A'
            ))/3
        @test rectangleoperator(lattice, CartesianIndex{LAT_DIM}(3,2),μ,ν) ≈
            real(tr(
                (7+7im)*B*(10+7im)*B*(4+5im)*A*
                conj(8+7im)*B'*conj(5+7im)*B'*conj(7+5im)*A'
            ))/3
        @test rectangleoperator(lattice, CartesianIndex{LAT_DIM}(1,3),μ,ν) ≈
            real(tr(
                (8+7im)*B*(2+7im)*B*(5+5im)*A*
                conj(3+7im)*B'*conj(9+7im)*B'*conj(8+5im)*A'
            ))/3
        @test rectangleoperator(lattice, CartesianIndex{LAT_DIM}(2,3),μ,ν) ≈
            real(tr(
                (9+7im)*B*(3+7im)*B*(6+5im)*A*
                conj(4+7im)*B'*conj(10+7im)*B'*conj(9+5im)*A'
            ))/3
        @test rectangleoperator(lattice, CartesianIndex{LAT_DIM}(3,3),μ,ν) ≈
            real(tr(
                (10+7im)*B*(4+7im)*B*(7+5im)*A*
                conj(2+7im)*B'*conj(8+7im)*B'*conj(10+5im)*A'
            ))/3
    end

    =#

    @testset "planarrectangularoperator coldstart" begin
        site = (x,y) -> CartesianIndex{LAT_DIM}(x,y)
        b = 3
        a = 2
        for (μ,ν) in [[1,2],[2,1]]
            for x in 1:L, y in 1:L
                @test planarrectangularoperator(cold_lattice,
                    site(x,y),μ,ν,b,a) == 1.0
            end
        end
    end
    
    @testset "plaquetteoperator 2D Lattice; Random Odd/Even Filling" begin
        a = 1
        μ=1
        ν=2
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(1,1),μ,ν,a,a) ≈ 
            real(tr((2+5im)*A*(3+7im)*B*conj(5+5im)*A'*conj(2+7im)*B'))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(2,1),μ,ν,a,a) ≈ 
            real(tr((3+5im)*A*(4+7im)*B*conj(6+5im)*A'*conj(3+7im)*B'))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(3,1),μ,ν,a,a) ≈ 
            real(tr((4+5im)*A*(2+7im)*B*conj(7+5im)*A'*conj(4+7im)*B'))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(1,2),μ,ν,a,a) ≈ 
            real(tr((5+5im)*A*(6+7im)*B*conj(8+5im)*A'*conj(5+7im)*B'))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(2,2),μ,ν,a,a) ≈ 
            real(tr((6+5im)*A*(7+7im)*B*conj(9+5im)*A'*conj(6+7im)*B'))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(3,2),μ,ν,a,a) ≈ 
            real(tr((7+5im)*A*(5+7im)*B*conj(10+5im)*A'*conj(7+7im)*B'))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(1,3),μ,ν,a,a) ≈ 
            real(tr((8+5im)*A*(9+7im)*B*conj(2+5im)*A'*conj(8+7im)*B'))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(2,3),μ,ν,a,a) ≈ 
            real(tr((9+5im)*A*(10+7im)*B*conj(3+5im)*A'*conj(9+7im)*B'))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(3,3),μ,ν,a,a) ≈ 
            real(tr((10+5im)*A*(8+7im)*B*conj(4+5im)*A'*conj(10+7im)*B'))/3
        
        μ=2
        ν=1
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(1,1),μ,ν,a,a) ≈ 
            real(tr(conj(2+5im)*A'*conj(3+7im)*B'*(5+5im)*A*(2+7im)*B))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(2,1),μ,ν,a,a) ≈ 
            real(tr(conj(3+5im)*A'*conj(4+7im)*B'*(6+5im)*A*(3+7im)*B))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(3,1),μ,ν,a,a) ≈ 
            real(tr(conj(4+5im)*A'*conj(2+7im)*B'*(7+5im)*A*(4+7im)*B))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(1,2),μ,ν,a,a) ≈ 
            real(tr(conj(5+5im)*A'*conj(6+7im)*B'*(8+5im)*A*(5+7im)*B))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(2,2),μ,ν,a,a) ≈ 
            real(tr(conj(6+5im)*A'*conj(7+7im)*B'*(9+5im)*A*(6+7im)*B))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(3,2),μ,ν,a,a) ≈ 
            real(tr(conj(7+5im)*A'*conj(5+7im)*B'*(10+5im)*A*(7+7im)*B))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(1,3),μ,ν,a,a) ≈ 
            real(tr(conj(8+5im)*A'*conj(9+7im)*B'*(2+5im)*A*(8+7im)*B))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(2,3),μ,ν,a,a) ≈ 
            real(tr(conj(9+5im)*A'*conj(10+7im)*B'*(3+5im)*A*(9+7im)*B))/3
        @test planarrectangularoperator(lattice,CartesianIndex{LAT_DIM}(3,3),μ,ν,a,a) ≈ 
            real(tr(conj(10+5im)*A'*conj(8+7im)*B'*(4+5im)*A*(10+7im)*B))/3
        
    end

    @testset "planarrectangularoperator 1x1 2D Lattice; Random Odd/Even Filling" begin
        site = (x,y) -> CartesianIndex{LAT_DIM}(x,y)
        # 1x1 loop operator is the same as the planarrectangularoperator      
        b = 1
        a = 1
        μ=1
        ν=2
        for i in 1:L, j in 1:L
            @test planarrectangularoperator(lattice,site(i,j),μ,ν,b,a) ≈
                plaquetteoperator(lattice,site(i,j),μ,ν) 
        end
    end

    @testset "planarrectangularoperator 1x1 3D Lattice; Random Odd/Even Filling" begin
        # Tests fail with random A and B, numerical error of incorrect algorithm? 
        A = ones(3,3)
        B = ones(3,3)
        B .*= 2.0
        g2(x) = isodd(x) ? A : B
        LAT_DIM = 3
        lattice = GaugeFields(LAT_DIM,MAT_DIM,L)

        site = (x,y,z) -> CartesianIndex{LAT_DIM}(x,y,z)


        for (j,index) in enumerate(CartesianIndices(lattice.links))
            for (k,directionindex) in enumerate(eachindex(lattice.links[index]))
                lattice.links[index][directionindex] *= ((j+1)+f(k)*1im)*g2(k)
            end
        end

        # 1x1 loop operator is the same as the planarrectangularoperator      
        b = 1
        a = 1
        μ=1
        ν=2
        for i in 1:L, j in 1:L, k in 1:L
            @test planarrectangularoperator(lattice,site(i,j,k),μ,ν,b,a) ≈
                plaquetteoperator(lattice,site(i,j,k),μ,ν) 
        end
    end

    @testset "planarrectangularoperator 1x1 4D Lattice; Random Odd/Even Filling" begin
        # Tests fail with random A and B, numerical error of incorrect algorithm? 
        A = ones(3,3)
        B = ones(3,3)
        B .*= 2.0
        g2(x) = isodd(x) ? A : B
        LAT_DIM = 4
        lattice = GaugeFields(LAT_DIM,MAT_DIM,L)

        site = (t,x,y,z) -> CartesianIndex{LAT_DIM}(t,x,y,z)

        for (j,index) in enumerate(CartesianIndices(lattice.links))
            for (k,directionindex) in enumerate(eachindex(lattice.links[index]))
                lattice.links[index][directionindex] *= ((j+1)+f(k)*1im)*g2(k)
            end
        end

        # 1x1 loop operator is the same as the planarrectangularoperator      
        b = 1
        a = 1
        μ=2
        ν=1
        for i in 1:L, j in 1:L, k in 1:L, ℓ in 1:L
            @test planarrectangularoperator(lattice,site(i,j,k,ℓ),μ,ν,b,a) ≈
                plaquetteoperator(lattice,site(i,j,k,ℓ),μ,ν) 
        end
    end

    
    @testset "planarrectangularoperator 2x1 2D Lattice; Random Odd/Even Filling" begin
        site = (x,y) -> CartesianIndex{LAT_DIM}(x,y)
        # 2x1 loop operator is the same as the rectangleoperator      
        b = 2
        a = 1
        μ=1
        ν=2
        for i in 1:L, j in 1:L
            @test planarrectangularoperator(lattice,site(i,j),μ,ν,b,a) ≈
                rectangleoperator(lattice,site(i,j),μ,ν) 
        end
    end

    @testset "planarrectangularoperator 2x1 4D Lattice; Random Odd/Even Filling" begin
        # Tests fail with random A and B, numerical error of incorrect algorithm? 
        C = A ./ norm(A)
        D = B ./ norm(B)
        g2(x) = isodd(x) ? C : D
        LAT_DIM = 4
        lattice = GaugeFields(LAT_DIM,MAT_DIM,L)

        site = (t,x,y,z) -> CartesianIndex{LAT_DIM}(t,x,y,z)

        for (j,index) in enumerate(CartesianIndices(lattice.links))
            for (k,directionindex) in enumerate(eachindex(lattice.links[index]))
                lattice.links[index][directionindex] *= ((j+1)+f(k)*1im)*g2(k)
            end
        end

        # 1x1 loop operator is the same as the planarrectangularoperator      
        b = 2
        a = 1
        μ=1
        ν=2
        for i in 1:L, j in 1:L, k in 1:L, ℓ in 1:L
            @test planarrectangularoperator(lattice,site(i,j,k,ℓ),μ,ν,b,a) ≈
                rectangleoperator(lattice,site(i,j,k,ℓ),μ,ν) 
        end
    end

    @testset "measureplaquette 2D Lattice; Cold Start" begin
        @test measureplaquette(cold_lattice) == 1.0 * L^LAT_DIM
    end

    @testset "measureplaquette 2D Lattice; Random Odd/Even Filling" begin
        average = real(tr(
            (2+5im)*A*(3+7im)*B*conj(5+5im)*A'*conj(2+7im)*B'
            + (3+5im)*A*(4+7im)*B*conj(6+5im)*A'*conj(3+7im)*B'
            + (4+5im)*A*(2+7im)*B*conj(7+5im)*A'*conj(4+7im)*B'
            + (5+5im)*A*(6+7im)*B*conj(8+5im)*A'*conj(5+7im)*B'
            + (6+5im)*A*(7+7im)*B*conj(9+5im)*A'*conj(6+7im)*B'
            + (7+5im)*A*(5+7im)*B*conj(10+5im)*A'*conj(7+7im)*B'
            + (8+5im)*A*(9+7im)*B*conj(2+5im)*A'*conj(8+7im)*B'
            + (9+5im)*A*(10+7im)*B*conj(3+5im)*A'*conj(9+7im)*B'
            + (10+5im)*A*(8+7im)*B*conj(4+5im)*A'*conj(10+7im)*B'
            ))/3
        @test measureplaquette(lattice) ≈ average
    end


    @testset "measurerectangle 2D Lattice; Cold Start" begin
        @test measurerectangle(cold_lattice) == 1.0 * L^LAT_DIM
    end

    @testset "measurerectangle 2D Lattice; Random Odd/Even Filling" begin
        # Wrong directions
        #average = ( 
        #    real(tr((2+7im)*B*(5+5im)*A*(6+5im)*A*
        #        conj(4+7im)*B'*conj(3+5im)*A'*conj(2+5im)*A'))
        #      + real(tr((3+7im)*B*(6+5im)*A*(7+5im)*A*
        #        conj(2+7im)*B'*conj(4+5im)*A'*conj(3+5im)*A'))
        #      + real(tr((4+7im)*B*(7+5im)*A*(5+5im)*A*
        #        conj(3+7im)*B'*conj(2+5im)*A'*conj(4+5im)*A'))
        #      + real(tr((5+7im)*B*(8+5im)*A*(9+5im)*A*
        #        conj(7+7im)*B'*conj(6+5im)*A'*conj(5+5im)*A')) 
        #      + real(tr((6+7im)*B*(9+5im)*A*(10+5im)*A*
        #        conj(5+7im)*B'*conj(7+5im)*A'*conj(6+5im)*A'))
        #      + real(tr((7+7im)*B*(10+5im)*A*(8+5im)*A*
        #        conj(6+7im)*B'*conj(5+5im)*A'*conj(7+5im)*A'))
        #      + real(tr((8+7im)*B*(2+5im)*A*(3+5im)*A*
        #        conj(10+7im)*B'*conj(9+5im)*A'*conj(8+5im)*A'))
        #      + real(tr((9+7im)*B*(3+5im)*A*(4+5im)*A*
        #        conj(8+7im)*B'*conj(10+5im)*A'*conj(9+5im)*A'))
        #      + real(tr((10+7im)*B*(4+5im)*A*(2+5im)*A*
        #        conj(9+7im)*B'*conj(8+5im)*A'*conj(10+5im)*A'))
        #    )/3
        #@test measurerectangle(lattice) ≈ average   

        average = real(tr(
            (2+7im)*B*(5+7im)*B*(8+5im)*A*
                conj(6+7im)*B'*conj(3+7im)*B'*conj(2+5im)*A'
            + (3+7im)*B*(6+7im)*B*(9+5im)*A*
            conj(7+7im)*B'*conj(4+7im)*B'*conj(3+5im)*A'
            + (4+7im)*B*(7+7im)*B*(10+5im)*A*
            conj(5+7im)*B'*conj(2+7im)*B'*conj(4+5im)*A'
            + (5+7im)*B*(8+7im)*B*(2+5im)*A*
            conj(9+7im)*B'*conj(6+7im)*B'*conj(5+5im)*A'
            + (6+7im)*B*(9+7im)*B*(3+5im)*A*
            conj(10+7im)*B'*conj(7+7im)*B'*conj(6+5im)*A'
            + (7+7im)*B*(10+7im)*B*(4+5im)*A*
            conj(8+7im)*B'*conj(5+7im)*B'*conj(7+5im)*A'
            + (8+7im)*B*(2+7im)*B*(5+5im)*A*
            conj(3+7im)*B'*conj(9+7im)*B'*conj(8+5im)*A'
            + (9+7im)*B*(3+7im)*B*(6+5im)*A*
            conj(4+7im)*B'*conj(10+7im)*B'*conj(9+5im)*A'
            + (10+7im)*B*(4+7im)*B*(7+5im)*A*
            conj(2+7im)*B'*conj(8+7im)*B'*conj(10+5im)*A'
        ))/3
        @test measurerectangle(lattice) ≈ average
    end

    
    @testset "measureplanarrectangular 2D Lattice; Random Odd/Even Filling" begin
         

        @testset "Plaquette measured" begin
            @test measureplanarrectangular(lattice, 1, 1) ≈
                measureplaquette(lattice)
        end
        @testset "Plaquette measured manual" begin
            average = real(tr(
                (2+5im)*A*(3+7im)*B*conj(5+5im)*A'*conj(2+7im)*B'
                + (3+5im)*A*(4+7im)*B*conj(6+5im)*A'*conj(3+7im)*B'
                + (4+5im)*A*(2+7im)*B*conj(7+5im)*A'*conj(4+7im)*B'
                + (5+5im)*A*(6+7im)*B*conj(8+5im)*A'*conj(5+7im)*B'
                + (6+5im)*A*(7+7im)*B*conj(9+5im)*A'*conj(6+7im)*B'
                + (7+5im)*A*(5+7im)*B*conj(10+5im)*A'*conj(7+7im)*B'
                + (8+5im)*A*(9+7im)*B*conj(2+5im)*A'*conj(8+7im)*B'
                + (9+5im)*A*(10+7im)*B*conj(3+5im)*A'*conj(9+7im)*B'
                + (10+5im)*A*(8+7im)*B*conj(4+5im)*A'*conj(10+7im)*B'
                ))/3
            @test measureplanarrectangular(lattice,1,1) ≈ average
        end
            
        @testset "Rectangle measured" begin      
            @test measureplanarrectangular(lattice, 2, 1) ≈
                measurerectangle(lattice)
        end
    end

    @testset "measureplanarrectangular 4D Lattice; Random Odd/Even Filling" begin
        #Tests fail with random A and B, numerical error of incorrect algorithm? 
        C = A ./ norm(A)
        D = B ./ norm(B)
        g2(x) = isodd(x) ? C : D
        LAT_DIM = 4
        lattice = GaugeFields(LAT_DIM,MAT_DIM,L)

        for (j,index) in enumerate(CartesianIndices(lattice.links))
            for (k,directionindex) in enumerate(eachindex(lattice.links[index]))
                lattice.links[index][directionindex] *= ((j+1)+f(k)*1im)*g2(k)
            end
        end

        # 1x1 loop operator is the same as the planarrectangularoperator      
        b = 2
        a = 1

        @testset "Plaquette measured" begin
            @test measureplanarrectangular(lattice, a, a) ≈
                measureplaquette(lattice)
        end
            
        @testset "Rectangle measured" begin      
            @test measureplanarrectangular(lattice, b, a) ≈
                measurerectangle(lattice)
        end
    end

    
    #=

    #=
    $$ \Gamma_\mu(x) = \sum_{\nu!=\mu}U_{\nu}(x+\hat{\mu})U_{\mu}^\dagger(x+\hat{\nu})U_{\nu}^\dagger(x) + U_{\nu}^\dagger(x+\hat{\mu}-\hat{\nu})U_{\mu}^\dagger(x-\hat{\nu})U_{\nu}(x-\hat{\nu})$$
    =#
    
    @testset "plaquettestaples 2D Lattice; Random Odd/Even Filling" begin        
        site = (x,y) -> CartesianIndex{LAT_DIM}(x,y)
        μ = 1
    
    
        @test plaquettestaples(lattice, site(1,1), μ) ≈ (
            (3+7im)*B*conj(5+5im)*A'*conj(2+7im)*B'
            + conj(9+7im)*B'*conj(8+5im)*A'*(8+7im)*B
        )

        @test plaquettestaples(lattice, site(1,2), μ) ≈ (
            (6+7im)*B*conj(8+5im)*A'*conj(5+7im)*B'
            + conj(3+7im)*B'*conj(2+5im)*A'*(2+7im)*B
            )

        @test plaquettestaples(lattice, site(1,3), μ) ≈ (
            (9+7im)*B*conj(2+5im)*A'*conj(8+7im)*B'
            + conj(6+7im)*B'*conj(5+5im)*A'*(5+7im)*B
            )

        @test plaquettestaples(lattice, site(2,1), μ) ≈ (
            (4+7im)*B*conj(6+5im)*A'*conj(3+7im)*B'
            + conj(10+7im)*B'*conj(9+5im)*A'*(9+7im)*B
            )

        @test plaquettestaples(lattice, site(2,2), μ) ≈ (
            (7+7im)*B*conj(9+5im)*A'*conj(6+7im)*B'
            + conj(4+7im)*B'*conj(3+5im)*A'*(3+7im)*B
            )

        @test plaquettestaples(lattice, site(2,3), μ) ≈ (
            (10+7im)*B*conj(3+5im)*A'*conj(9+7im)*B'
            + conj(7+7im)*B'*conj(6+5im)*A'*(6+7im)*B
            )

        @test plaquettestaples(lattice, site(3,1), μ) ≈ (
            (2+7im)*B*conj(7+5im)*A'*conj(4+7im)*B'
            + conj(8+7im)*B'*conj(10+5im)*A'*(10+7im)*B
            )

        @test plaquettestaples(lattice, site(3,2), μ) ≈ (
            (5+7im)*B*conj(10+5im)*A'*conj(7+7im)*B'
            + conj(2+7im)*B'*conj(4+5im)*A'*(4+7im)*B
            )
        
        @test plaquettestaples(lattice, site(3,3), μ) ≈ (
            (8+7im)*B*conj(4+5im)*A'*conj(10+7im)*B'
            + conj(5+7im)*B'*conj(7+5im)*A'*(7+7im)*B
            )
    end

    @testset "plaquettestaples 3D Lattice; Random Odd/Even Filling" begin  
        # Need to test the running over orientations when there are more that 
        # one plane contributing to the staple    
        site = (x,y,z) -> CartesianIndex{LAT_DIM}(x,y,z)
        μ = 1
        
        # Tests fail with random A and B, numerical error of incorrect algorithm? 
        A = ones(3,3)
        B = ones(3,3)
        B .*= 2.0
        g2(x) = isodd(x) ? A : B
        LAT_DIM = 3
        lattice = GaugeFields(LAT_DIM,MAT_DIM,L)

        for (j,index) in enumerate(CartesianIndices(lattice.links))
            for (k,directionindex) in enumerate(eachindex(lattice.links[index]))
                lattice.links[index][directionindex] *= ((j+1)+f(k)*1im)*g2(k)
            end
        end
    
        Γ = (
            (16+7im)*B*conj(18+5im)*A'*conj(15+7im)*B' 
            + conj(13+7im)*B'*conj(12+5im)*A'*(12+7im)*B'
            + (16+5im)*A*conj(24+5im)*A'*conj(15+5im)*A' 
            + conj(7+5im)*A'*conj(6+5im)*A'*(6+5im)*A 
            )
        @test plaquettestaples(lattice,site(2,2,2),μ)[begin] ≈ Γ[begin]
    end

    @testset "rectanglestaples 2D Lattice; Random Odd/Even Filling" begin        
        site = (x,y) -> CartesianIndex{LAT_DIM}(x,y)
        μ = 1
    
    
        @test rectanglestaples(lattice, site(2,2), μ) ≈ (
            (7+5im)*A*(5+7im)*B*(10-5im)*A'*(9-5im)*A'*(6-7im)*B'
            + (7+5im)*A*(2-7im)*B'*(4-5im)*A'*(3-5im)*A'*(3+7im)*B
            + (7+7im)*B*(9-5im)*A'*(8-5im)*A'*(5-7im)*B'*(5+5im)*A
            + (4-7im)*B'*(3-5im)*A'*(2-5im)*A'*(2+7im)*B*(5+5im)*A
            + (7+7im)*B*(10+7im)*B*(3-5im)*A'*(9-7im)*B'*(6-7im)*B'
            + (4-7im)*B'*(10-7im)*B'*(9-5im)*A'*(9+7im)*B*(3+7im)*B
        )
    end

    

    @testset "improvedstaples 2D Lattice; Random Odd/Even Filling" begin
        site = (x,y) -> CartesianIndex{LAT_DIM}(x,y)
        μ = 1
        test_u0 = 3.4
        
        test_values = (
            a = 0., 
            β = 0.,
            u0 = test_u0,
            ϵ = 0.,
            lattice_dimension = 0,
            side_length = 0, 
            matrix_dimension = 0, 
            number_matrices = 0, 
            N_cor = 0,
            N_cf = 0, 
            N_hits = 0
        )

        params = Params{Float64,Int64}(test_values...)

        coef1 = 5.0 / (3.0 * test_u0^4)
        coef2 = 1.0 / (12.0 * test_u0^6)

        ΓP = (
            (7+7im)*B*conj(9+5im)*A'*conj(6+7im)*B'
            + conj(4+7im)*B'*conj(3+5im)*A'*(3+7im)*B
            )

        ΓR = (
            (7+5im)*A*(5+7im)*B*(10-5im)*A'*(9-5im)*A'*(6-7im)*B'
            + (7+5im)*A*(2-7im)*B'*(4-5im)*A'*(3-5im)*A'*(3+7im)*B
            + (7+7im)*B*(9-5im)*A'*(8-5im)*A'*(5-7im)*B'*(5+5im)*A
            + (4-7im)*B'*(3-5im)*A'*(2-5im)*A'*(2+7im)*B*(5+5im)*A
            + (7+7im)*B*(10+7im)*B*(3-5im)*A'*(9-7im)*B'*(6-7im)*B'
            + (4-7im)*B'*(10-7im)*B'*(9-5im)*A'*(9+7im)*B*(3+7im)*B
            )

        @test improvedstaples(lattice,site(2,2),μ,params) ≈ (coef1 * ΓP - coef2 * ΓR)
    end
    =#
end #module TestOperators
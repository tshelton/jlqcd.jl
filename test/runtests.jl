using JLQCD
using Test

#@testset "JLQCD.jl" begin
#    # TODO
#end
searchdir(path, key) = filter(x->occursin(key,x), readdir(path))
excludefile(files, file) = filter(x->!occursin(file,x), files)

my_tests = searchdir(".",".jl") |> x->excludefile(x, "runtests.jl")

my_tests = ["operators.jl"]

for my_test in my_tests
    include(my_test)
end
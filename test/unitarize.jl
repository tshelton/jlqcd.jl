module TestUnitarize
    using Test
    using LinearAlgebra
    using Random
    using Distributions: Uniform
    using FromFile: @from 
    @from "../src/unitarize.jl" import Unitarize: unitarize, specialunitarize

    Random.seed!(12345)

    n = 10
    areal = randn(n,n)/2
    aimg  = randn(n,n)/2
    #@testset "Unitarize" begin
    @testset for eltya in (Float32, Float64, ComplexF32, ComplexF64)#, BigFloat) 
        # specialunitarize is not numerically accurate enough for BigFloat, TODO
        raw_a = eltya == Int ? rand(1:7, n, n) : convert(Matrix{eltya}, eltya <: Complex ? complex.(areal, aimg) : areal)
        ϵ = eps(abs(float(one(eltya))))
        @testset "unitarize on a number" begin
            α = rand(eltya)
            z = unitarize(α)
            @test (z*conj(z))[begin,begin] ≈ one(eltya)
        end

        @testset "unitarize on a matrix" begin
            Z = unitarize(raw_a)
            @test Z*Z' ≈ Matrix{eltya}(I,n,n)
        end
        @testset "specialunitarize on a matrix" begin
            Z = specialunitarize(raw_a)
            @test det(Z) ≈ one(eltya) atol=3000ϵ
        end
    end
    #end
end
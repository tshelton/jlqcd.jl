module TestLattice
    using Test
    using LinearAlgebra
    using FromFile: @from 
    @from "../src/lattice.jl" import Lattice: nearestneighbors, GaugeFields
    
    ##################################
    # function nearestneighbors tests
    ##################################

    @testset "Nearest Neighbors 1D" begin
        """
        For one spacetime dimension, test that the nearest neighbors are calculated correctly for one middle lattice point and the first lattice point and last lattice point (to test periodic calculations).  
        """
        # One dimensional lattice tests, 3 dim discritization
        N_DIMS = 1
        index = CartesianIndex{N_DIMS}(1) # First
        @test isequal(nearestneighbors(index,3), [
            CartesianIndex{N_DIMS}(2),
            CartesianIndex{N_DIMS}(3)
        ])
        index = CartesianIndex{N_DIMS}(2) # Middle
        @test isequal(nearestneighbors(index,3), [
            CartesianIndex{N_DIMS}(3),
            CartesianIndex{N_DIMS}(1)
        ])
        index = CartesianIndex{N_DIMS}(3) # Last
        @test isequal(nearestneighbors(index,3), [
            CartesianIndex{N_DIMS}(1),
            CartesianIndex{N_DIMS}(2)
        ])
    end

    @testset "Nearest Neighbors 2D" begin
        """
        For two spacetime dimensions, test that the nearest neighbors are calculated correctly for one middle lattice point and the first lattice point and last lattice point (to test periodic calculations).  
        """
        # Two dimensional lattice tests, 8 dim discritization
        N_DIMS = 2
        index = CartesianIndex{N_DIMS}(1,1) # First
	    @test isequal(nearestneighbors(index,8), [
            CartesianIndex{N_DIMS}(2,1),
            CartesianIndex{N_DIMS}(1,2),
            CartesianIndex{N_DIMS}(8,1),
            CartesianIndex{N_DIMS}(1,8)
        ])
        index = CartesianIndex{N_DIMS}(4,5) # Middle-ish
	    @test isequal(nearestneighbors(index,8), [
            CartesianIndex{N_DIMS}(5,5),
            CartesianIndex{N_DIMS}(4,6),
            CartesianIndex{N_DIMS}(3,5),
            CartesianIndex{N_DIMS}(4,4)
        ])
        index = CartesianIndex{N_DIMS}(8,8) # Last
	    @test isequal(nearestneighbors(index,8), [
            CartesianIndex{N_DIMS}(1,8),
            CartesianIndex{N_DIMS}(8,1),
            CartesianIndex{N_DIMS}(7,8),
            CartesianIndex{N_DIMS}(8,7)
        ])
    end

    @testset "Nearest Neighbors 4D" begin
        """
        For four spacetime dimensions, test that the nearest neighbors are calculated correctly for one middle lattice point and the first lattice point and last lattice point (to test periodic calculations).  
        """
        # Four dimension lattice tests, 3 dim discritization
        N_DIMS = 4 # Temporal + Spacial Dimensions
        index = CartesianIndex{N_DIMS}(1,1,1,1) # First
        @test isequal(nearestneighbors(index,3), [
            CartesianIndex{N_DIMS}(2,1,1,1),
            CartesianIndex{N_DIMS}(1,2,1,1),
            CartesianIndex{N_DIMS}(1,1,2,1),
            CartesianIndex{N_DIMS}(1,1,1,2),
            CartesianIndex{N_DIMS}(3,1,1,1),
            CartesianIndex{N_DIMS}(1,3,1,1),
            CartesianIndex{N_DIMS}(1,1,3,1),
            CartesianIndex{N_DIMS}(1,1,1,3)
        ])
        index = CartesianIndex{N_DIMS}(3,3,3,3) # Last
        @test isequal(nearestneighbors(index,3), [
            CartesianIndex{N_DIMS}(1,3,3,3),
            CartesianIndex{N_DIMS}(3,1,3,3),
            CartesianIndex{N_DIMS}(3,3,1,3),
            CartesianIndex{N_DIMS}(3,3,3,1),
            CartesianIndex{N_DIMS}(2,3,3,3),
            CartesianIndex{N_DIMS}(3,2,3,3),
            CartesianIndex{N_DIMS}(3,3,2,3),
            CartesianIndex{N_DIMS}(3,3,3,2)
        ])
        index = CartesianIndex{N_DIMS}(2,2,2,2) # Middle
        @test isequal(nearestneighbors(index,3), [
            CartesianIndex{N_DIMS}(3,2,2,2),
            CartesianIndex{N_DIMS}(2,3,2,2),
            CartesianIndex{N_DIMS}(2,2,3,2),
            CartesianIndex{N_DIMS}(2,2,2,3),
            CartesianIndex{N_DIMS}(1,2,2,2),
            CartesianIndex{N_DIMS}(2,1,2,2),
            CartesianIndex{N_DIMS}(2,2,1,2),
            CartesianIndex{N_DIMS}(2,2,2,1)
        ])
        index = CartesianIndex{N_DIMS}(1,2,3,2) # Mixed
        @test isequal(nearestneighbors(index,3), [
            CartesianIndex{N_DIMS}(2,2,3,2),
            CartesianIndex{N_DIMS}(1,3,3,2),
            CartesianIndex{N_DIMS}(1,2,1,2),
            CartesianIndex{N_DIMS}(1,2,3,3),
            CartesianIndex{N_DIMS}(3,2,3,2),
            CartesianIndex{N_DIMS}(1,1,3,2),
            CartesianIndex{N_DIMS}(1,2,2,2),
            CartesianIndex{N_DIMS}(1,2,3,1)
        ])
    end
       
    ##################################
    # struct GaugeFields tests
    ##################################

    @testset "GaugeFields Nearest Neighbor Generation" begin
        L = 3 # Lattice length
        spacetime_dim = 4
        gauge_dim = 3
        my_lattice = GaugeFields(spacetime_dim,gauge_dim,L)
        # Test that each neighbor is correctly created
        for t in 1:L, x in 1:L, y in 1:L, z in 1:L
            nrstnghbrs = [ 
                CartesianIndex(mod1(t+1,L),x,y,z),
                CartesianIndex(t,mod1(x+1,L),y,z),
                CartesianIndex(t,x,mod1(y+1,L),z),
                CartesianIndex(t,x,y,mod1(z+1,L)),
                CartesianIndex(mod1(t-1,L),x,y,z),
                CartesianIndex(t,mod1(x-1,L),y,z),
                CartesianIndex(t,x,mod1(y-1,L),z),
                CartesianIndex(t,x,y,mod1(z-1,L))
            ]
            #@test isequal(lattice.nearest_neighbors[t,x,y,z][:], Neighbors(CartesianIndex{N_DIMS}(t,x,y,z),L)[:])
            @test isequal(my_lattice.nearest_neighbors[t,x,y,z], nrstnghbrs)
        end
    end

    @testset "GaugeFields Cold Start 1D SU(3)" begin
        L = 3 # Lattice length
        spacetime_dim = 1
        gauge_dim = 3
        my_lattice = GaugeFields(spacetime_dim,gauge_dim,L)

        for index in CartesianIndices(my_lattice.links)
            @test isequal(my_lattice.links[index], 
                fill(Matrix{ComplexF64}(I,gauge_dim,gauge_dim),spacetime_dim)) 
        end
    end

    @testset "GaugeFields Cold Start 1D SO(3)" begin
        L = 3 # Lattice length
        spacetime_dim = 1
        gauge_dim = 3
        my_lattice = GaugeFields(spacetime_dim,gauge_dim,L,Matrix{Float64})

        for index in CartesianIndices(my_lattice.links)
            @test isequal(my_lattice.links[index], 
                fill(Matrix{Float64}(I,gauge_dim,gauge_dim),spacetime_dim)) 
        end
    end

    @testset "GaugeFields Cold Start 4D SU(3)" begin
        L = 3 # Lattice length
        spacetime_dim = 4
        gauge_dim = 3
        my_lattice = GaugeFields(spacetime_dim,gauge_dim,L)

        for index in CartesianIndices(my_lattice.links)
            @test isequal(my_lattice.links[index], 
                fill(Matrix{ComplexF64}(I,gauge_dim,gauge_dim),spacetime_dim)) 
        end
    end

    @testset "GaugeFields Cold Start 4D SU(4)" begin
        L = 3 # Lattice length
        spacetime_dim = 4
        gauge_dim = 4
        my_lattice = GaugeFields(spacetime_dim,gauge_dim,L)

        for index in CartesianIndices(my_lattice.links)
            @test isequal(my_lattice.links[index], 
                fill(Matrix{ComplexF64}(I,gauge_dim,gauge_dim),spacetime_dim)) 
        end
    end

    @testset "GaugeFields Links Set/Get" begin
        L = 3 # Lattice length
        spacetime_dim = 1
        gauge_dim = 3
        my_lattice = GaugeFields(spacetime_dim,gauge_dim,L)

        my_lattice.links[begin][begin] = zeros(ComplexF64, gauge_dim, gauge_dim)

        @test isequal(my_lattice.links[begin][begin], 
            zeros(ComplexF64, gauge_dim, gauge_dim))

        my_lattice = GaugeFields(spacetime_dim,gauge_dim,L,Matrix{Float64})

        my_lattice.links[begin][begin] = zeros(Float64, gauge_dim, gauge_dim)

        @test isequal(my_lattice.links[begin][begin], 
            zeros(Float64, gauge_dim, gauge_dim))
    end
end
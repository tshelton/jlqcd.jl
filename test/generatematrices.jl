module TestGenerateMatrices
    using Test
    using LinearAlgebra
    using Random
    using Distributions: Uniform
    using FromFile: @from 
    @from "../src/generatematrices.jl" import GenerateMatrices: generaterandomSUNmatrices

    Random.seed!(1234)
    gauge_dim = 3
    number_matrices = 50
    ϵ = 0.24
    matrices = generaterandomSUNmatrices(gauge_dim,number_matrices,ϵ)

    @testset "generaterandomSUNmatrices Unitarity" begin
        for U in matrices
            @test U*U' ≈ I # Check that each random matrix is unitary
        end
    end

    @testset "generaterandomSUNmatrices Inverse Inclusion" begin
        for U in matrices[begin:2:end] # Only need to each every-other element by construction
            @test U' in matrices # Check that the inverse is included
        end
    end

    @testset "generaterandomSUNmatrices Unit Determinant" begin
        for U in matrices
            @test det(U) ≈ 1 # Check each is in SU
        end
    end
end
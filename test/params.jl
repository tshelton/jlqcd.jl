module TestParameters
    using Test
    using DelimitedFiles
    using FromFile: @from
    @from "../src/params.jl" import Parameters: Params 

    test_values = (
        a = 1., # a 
        β = 5.5, # β 
        u0 = 0.0, # u₀
        ϵ = 0.24, # ϵ
        lattice_dimension = 4, # Lattice dimension
        side_length = 8, # Temporal/Spacial Length 
        matrix_dimension = 3, #  
        number_matrices = 50, #  Number of Lie Group spanning set of matrices
        N_cor = 20, # Number of runs to decorralate data
        N_cf = 400, # Number of runs 
        N_hits = 10 # Number of updates for the local link variable
    )

    @testset "Params sets/gets" begin
        test_params = Params{Float64,Int64}(test_values...)
        for field in fieldnames(typeof(test_params))
            @test getfield(test_params,field) == getfield(test_values,field) 
        end
    end

    @testset "Params loads from file" begin
    
        filename = "test_params.txt"
        open(filename, "w") do file
            for value in test_values
                println(file, value)
            end
        end

        @testset "Params filename contructor" begin
            test_params = Params(filename)
            for field in fieldnames(typeof(test_params))
                @test getfield(test_params,field) == getfield(test_values,field)
            end
        end

        @testset "Params filename no-file" begin
            @test_throws ArgumentError Params("notfilename.txt")
        end

        rm(filename)
    end
end